<?php namespace Kms\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model {
    protected $fillable = [
        'title',
        'content',
        'datetime'
    ];

    public function user()
    {
        $this->belongsToMany('users');
    }
}