<?php namespace Kms\Models;

use Cartalyst\Sentinel\Users\EloquentUser as SentinelEloquentUser;
use Cartalyst\Sentinel\Users\UserInterface;

class User extends SentinelEloquentUser implements UserInterface {

    /**
     * {@inheritDoc}
     */
    protected $fillable = [
        'email',
        'password',
        'permissions',
        'first_name',
        'last_name',
    ];

    public function articles()
    {
        $this->hasMany('articles');
    }

}
