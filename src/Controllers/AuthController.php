<?php namespace Kms\Controllers;

use View;
use Input;
use Validator;
use Auth;
use Redirect;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;


class AuthController extends BaseController {

	/**
	 * Show the form for authenticating an user.
	 *
	 * @return \Response
	 */
	public function getLogin()
	{
		return View::make('auth.login');
	}

	/**
	 * Persist the session for an authenticated user.
	 *
	 * @return \Response
	 */
	public function postLogin()
	{
        $errorMessage = '';
		try
        {
            $input = Input::all();

            $rules = [
                'email' => 'required|email',
                'password' => 'required'
            ];

            $validation = Validator::make($input, $rules);

            if ($validation->fails())
            {
                return \Redirect::back()->withInput()->withErrors($validation->errors());
            }

            if (Auth::authenticate(Input::all()))
            {
                return \Redirect::intended('/');
            }

            $errorMessage = "Ongeldig e-mailadres of wachtwoord.";
        }
        catch (NotActivatedException $e)
        {
            $errorMessage = 'Dit gebruikersaccount is niet geactiveerd.';
        }
        catch (ThrottlingException $e)
        {
            $delay = $e->getDelay();
            $errorMessage = "Uw gebruikersaccount is geblokkeerd voor {$delay} seconde(s).";
        }
        return \Redirect::back()->withInput()->withErrors($errorMessage);
	}

    public function getLogout()
    {
        Auth::logout();
        return \Redirect::home();
    }
}
