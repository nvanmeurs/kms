<?php
/**
 * Part of the Sentinel package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the license.txt file.
 *
 * @package    Sentinel
 * @version    1.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2014, Cartalyst LLC
 * @link       http://cartalyst.com
 */

use Carbon\Carbon;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigrationCartalystSentinelRenameAlterGroupsUsersPivot extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::rename('users_groups', 'groups_users');

		Schema::table('groups_users', function(Blueprint $table)
		{
			$table->nullableTimestamps();
		});

		$now = Carbon::now();

		DB::table('groups_users')->update([
			'created_at' => $now,
			'updated_at' => $now,
		]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('groups_users', function(Blueprint $table)
		{
			$table->dropTimestamps();
		});

		Schema::rename('groups_users', 'users_groups');
	}

}
