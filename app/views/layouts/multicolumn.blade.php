@extends('layouts.master')

@section('column_css')
<link rel="stylesheet" media="screen,projection" type="text/css" href="/css/2-col.css" />
@stop

@section('navigation')
<?php $navigation = (isset($navigation)) ? $navigation : true; ?>
@if ($navigation)
    @include('layouts._partials.navigation')
@endif
@stop

@section('sidebar')
    @include('layouts._partials.sidebar')
@stop