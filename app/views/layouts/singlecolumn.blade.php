@extends('layouts.master')

@section('column_css')
<link rel="stylesheet" media="screen,projection" type="text/css" href="/css/1-col.css" />
@stop

@section('navigation')
<?php $navigation = (isset($navigation)) ? $navigation : true; ?>
@if ($navigation)
    @include('layouts._partials.navigation')
@endif
@stop