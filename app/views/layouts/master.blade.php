<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="/css/reset.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="/css/main.css" />
    @yield('column_css', '')
    <!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/main-msie.css" /><![endif]-->
    <link rel="stylesheet" media="screen,projection" type="text/css" href="/css/style.css" />
    <link href="/css/demo_page.css" rel="stylesheet" type="text/css" media="all" />
    <!-- <link href="/css/demo_table.css" rel="stylesheet" type="text/css" media="all" />-->
    <link href="/css/demo_table_jui.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/css/smoothness/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" media="all" />
    @yield('styles', '')
    <script type="text/javascript" language="javascript" src="/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" language="javascript" src="/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" charset="utf-8">
        $(function() {
            $("input[type=submit], .button, button").button();
            $("#accordion").accordion();
        });
    </script>
    @yield('scripts', '')
    <title>MvEa Kleding Management Systeem</title>
</head>

<body>

<div id="main">

    <!-- Header -->
    <div id="header">

        <p><img src="/img/logo.png" alt="logo" style="float:left;margin-left: -12px; margin-top: -12px;" /></p>
        <img style='float:left;margin-left: 142px; margin-top: 4px;' src='/img/banner_small.png' />
        <!-- <div id="slogan"></div> -->

    </div> <!-- /header -->

    <hr class="noscreen" />

    <!-- Navigation -->
    <div id="nav" class="box">

        <ul>
            @yield('navigation', '')
        </ul><!-- /feeds -->

    </div>
    <!-- /nav -->

    <hr class="noscreen" />

    <!-- Columns -->
    <div id="cols">
        <div id="cols-in" class="box">
            @yield('sidebar', '')

            <!-- Content -->
            <div id="content">


                <h2 class="title-01">
                    @section('title')
                        Home
                    @show
                </h2>

                <div class="in">

                    <div class="cols5050 box">

                    @yield('content')
                    </div> <!-- /cols5050 -->

                </div> <!-- /in -->

            </div><!-- /content -->

            <hr class="noscreen" />



        </div> <!-- /cols-in -->
    </div> <!-- /cols -->
    <hr class="noscreen" />

    <!-- Footer -->
    <div id="footer" class="box">

        <p class="f-right">

        </p>

        <p class="f-left">Copyright &copy; 2012 MVEA</p>

    </div> <!-- /box -->

</div> <!-- /main -->

</body>
</html>