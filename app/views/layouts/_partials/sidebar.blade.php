<?php $user = Auth::check(); ?>
<div id="aside">
    <div class="in">
        <dl id="news" class="box">
            <dt style=" background-color: green; color:white; font-weight:bold;padding:3px;">Gegevens contactpersoon</dt>
            <dd>
                <table style=" margin-top: -13px">
                    <tr><td>Email:</td><td> {{ $user->email }}<br /></td></tr>
                    <tr><td>Adres:</td><td>
                            {{ $user->streetname }} {{ $user->appartmentno }}</td></tr>
                    <br />
                    <tr><td>Postcode:</td><td> {{ $user->zipcode }}<br /></td></tr>
                    <tr><td>Woonplaats:</td><td> {{ $user->city }}<br /></td></tr>
                    <tr><td>Telefoon:</td><td> @telefoon<br /></td></tr>
                </table>
            </dd>
            </dl>
            </div>
    </div>