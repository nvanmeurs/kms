@extends('layouts.multicolumn')

@section('title')
    Nieuwsartikelen
@stop

@section('content')
<table cellpadding="0" cellspacing="0" border="0" class="display" id="table">
    <thead>
    <tr>
        <th>Nummer</th>
        <th>Titel</th>
        <th>Bericht</th>
        <th>Datum</th>
        <th>Acties</th>
    </tr>
    </thead>

    <tbody>
    <?php
    $nieuwsitems[] = [
        'NieuwsitemID' => 1,
        'Titel' => "Eerste nieuwsbericht",
        'Bericht' => "LOREM IPSUM",
        'Datum' => '01-07-2014'
    ];

    $nieuwsitems[] = [
        'NieuwsitemID' => 2,
        'Titel' => "Tweede nieuwsbericht",
        'Bericht' => "IPSUM LOREM",
        'Datum' => '30-06-2014'
    ];

    foreach ($articles as $nieuwsitem)
    {
            ?>
            <tr>
                <td>{{{ $nieuwsitem['id'] }}}</td>
                <td>{{{ $nieuwsitem['title'] }}}</td>
                <td>{{{ substr($nieuwsitem['content'], 0, 25) }}}...</td>
                <td>{{{ $nieuwsitem['datetime'] }}}</td>
                <td>
                    <a href='#'><img src='/img/delete-icon.png' alt='verwijderen' title="verwijderen"/></a>
                    <a href='#'><img src='/img/edit-icon.png' alt='wijzigen' title="wijzigen"/></a>
                    <a href='#'><img src='/img/view-icon.png' alt='bekijken' title="bekijken"/></a>
                </td>
            </tr>
        <?php
    }
    ?>
    </tbody>
</table>
@stop