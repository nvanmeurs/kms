@extends('layouts.singlecolumn')

<?php
$navigation = false;
?>

@section('title')
    Inloggen
@stop

@section('content')

@if($errors->has())
    <div id="error">
        @foreach ($errors->all() as $error)
            <div>{{ $error }}</div>
        @endforeach
    </div>
@endif

<div id="form">
    <form name="login" method="POST">
        <fieldset class="info_fieldset">
            <div id="fields">
                <label for="email">Email</label>
                <input class="textbox" type="text" name="email" value="" />

                <label for="password">Wachtwoord</label>
                <input class="textbox" type="password" name="password" value="" />

                <label>&nbsp;</label>&nbsp;
                <input class="button" type="submit" name="login" value="Inloggen" />&nbsp;

                <a class="button" href="#">Wachtwoord vergeten?</a>
            </div>
        </fieldset>
    </form>
</div>
@stop
