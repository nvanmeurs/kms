<?php
use Faker\Factory;
use Kms\Models\Article;

class ArticleSeeder extends Seeder {
    public function run()
    {
        DB::table('articles')->truncate();

        $fake = Factory::create();
        for ($i = 0; $i <= 20; $i++)
        {
            Article::create([
                'title' => $fake->sentence(),
                'content' => $fake->paragraph(),
                'datetime' => $fake->dateTime,
                'user_id' => 1
            ]);
        }
    }
} 