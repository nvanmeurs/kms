<?php
use Faker\Factory;
use Kms\Models\User;

class UserSeeder extends Seeder {
    public function run()
    {
        DB::table('users')->truncate();

        $fake = Factory::create();

        User::create([
            'email' => 'nvmeurs@outlook.com',
            'password' => Hash::make('123456'),
            'streetname' => 'Evertsenstraat',
            'appartmentno' => '90',
            'zipcode' => '2315SN',
            'city' => 'Leiden'
        ]);

        for ($i = 0; $i < 20; $i++)
        {
            User::create([
                'email' => $fake->safeEmail,
                'password' => Hash::make('123456'),
                'streetname' => $fake->streetName,
                'appartmentno' => $fake->buildingNumber,
                'zipcode' => $fake->postcode,
                'city' => $fake->city
            ]);
        }

        User::create([
            'email' => 'test@kms.nl',
            'password' => Hash::make('123456'),
        ]);
    }
} 