<?php

Route::get('/', ['before' => 'auth', 'as' => 'home', function()
{
    return View::make('articles.index');
}]);

Route::group(['namespace' => 'Kms\Controllers'], function()
{
    Route::get('inloggen', ['as' => 'login.form', 'uses' => 'AuthController@getLogin']);
    Route::post('inloggen', ['as' => 'login.post', 'uses' => 'AuthController@postLogin']);
    Route::get('uitloggen', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);

    Route::group(['before' => 'auth'], function()
    {
        Route::get('/', ['as' => 'home', 'uses' => 'ArticlesController@index']);
        Route::get('/nieuws', ['as' => 'nieuws', 'uses' => 'ArticlesController@index']);
    });
});